import SpecialThanks from "./specialThanks";
import { author } from "../../../static/thanksInformations";
import AuthorInformations from "./authorInformations";

function Contact() {
  return (
    <section id="main" class="container contact_main">
      <div class="docs-section">
        <h6 class="docs-header">{author.title}</h6>
        <p>{author.text}</p>
        <AuthorInformations></AuthorInformations>
      </div>
      <div class="docs-section">
        <h6 class="docs-header">Special thanks: </h6>
        <SpecialThanks></SpecialThanks>
      </div>
    </section>
  );
}

export default Contact;
