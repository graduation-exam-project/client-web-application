import React, { Component } from "react";
import Form from "./form";
import Stern from "./stern";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      view: 0
    };
  }
  render() {
    if (this.state.view === 0)
      return (
        <section id="main" class="container">
          <div class="docs-section">
            <h6 class="docs-header">Home</h6>
            <label for="exampleRecipientInput">Choose format: </label>
            <select class="u-full-width" id="exampleRecipientInput">
              <option value="Option 1">Unformal letter</option>
            </select>
            <Stern></Stern>
            <Form
              changeView={val => {
                this.setState({ view: val });
              }}
            ></Form>
          </div>
        </section>
      );
    else
      return (
        <section id="main" class="container">
          <div class="docs-section">
            <Form
              changeView={val => {
                this.setState({ view: val });
              }}
            ></Form>
          </div>
        </section>
      );
  }
}

export default Home;
