function Header() {
  return (
    <header class="header">
      <h2 class="title">GRADUATION TESTER</h2>
      <div class="header-spacer"></div>
    </header>
  );
}

export default Header;
