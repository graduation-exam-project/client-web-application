import tableItems from "../../../static/thanksInformations";

function SpecialThanks() {
  const tableRows = tableItems.map(row => (
    <tr key={"row_" + row.name}>
      <td>{row.name}</td>
      <td>
        <a href={row.full_url}>{row.url}</a>
      </td>
      <td class="mobile-hidden">{row.meaning}</td>
    </tr>
  ));

  return (
    <table class="u-full-width">
      <thead>
        <tr key="row_head">
          <th>Name</th>
          <th>Website</th>
          <th class="mobile-hidden">Meaning</th>
        </tr>
      </thead>
      <tbody>{tableRows}</tbody>
    </table>
  );
}

export default SpecialThanks;
