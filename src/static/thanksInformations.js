const tableItems = [
  {
    name: "Preact",
    url: "preactjs.com",
    full_url: "https://preactjs.com",
    meaning: "Small libary to make single-page applications."
  },
  {
    name: "Skeleton",
    url: "getskeleton.com",
    full_url: "http://getskeleton.com/",
    meaning: "Light css utility for simple and fast UI."
  },
  {
    name: "Language tool",
    url: "languagetool.org",
    full_url: "https://languagetool.org/",
    meaning: "Java library for spell and grammer checking."
  },
  {
    name: "Compromise",
    url: "compromise.cool",
    full_url: "http://compromise.cool/",
    meaning: "NodeJS library for Neural Language Processing."
  }
];

export default tableItems;

export const author = {
  title: "About author:",
  text:
    "I'm software developer, currently studying Computing systems on secondary technical school."
};

export const authorInformations = {
  name: "Radek Kučera",
  email: "radakuceru@gmail.com",
  phone: "+420732231746"
};
