import "./style/normalize";
import "./style/skeleton";
import "./index.css";

import App from "./components/app";

export default App;
