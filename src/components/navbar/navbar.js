import NavbarItem from "./navbarItem";

function Header() {
  return (
    <nav class="navbar">
      <div class="container">
        <ul class="navbar-list container">
          <NavbarItem link="/" title="HOME"></NavbarItem>
          <NavbarItem link="/about" title="ABOUT"></NavbarItem>
          <NavbarItem link="/contact" title="CONTACT"></NavbarItem>
        </ul>
      </div>
    </nav>
  );
}

export default Header;
