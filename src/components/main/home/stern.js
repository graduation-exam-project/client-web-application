import React from "react";
import topics from "../../../static/topics";

function Stern() {
  return (
    <div>
      <ul>
        <li>
          <b>{topics.topic}</b>
        </li>

        <li>
          <b>Paragraph topics:</b>
          <ol>
            {topics.paragraphTopics.map(para => (
              <li key={topics.paragraphTopics.indexOf(para)}>{para}</li>
            ))}
          </ol>
        </li>
        <li>
          <b>Text lenght is 120-150 words.</b>
        </li>
      </ul>
    </div>
  );
}

export default Stern;
