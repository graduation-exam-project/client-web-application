import Header from "./header/header";
import Navbar from "./navbar/navbar";
import Main from "./main/main";

function App() {
  return (
    <div class="container">
      <Header></Header>
      <Navbar></Navbar>
      <Main></Main>
    </div>
  );
}

export default App;
