const text =
  "Hi Lucy,\n\nI‘m writing to you because I have just moved to a new flat in the city. I‘d like to tell you some news about it because it‘s just great in here.\n\nOur new flat‘s very large and fully furnished. It’s also equipped with appliances. There are five spacious rooms, a modern kitchen and a balcony with a beautiful view of the city. The place is much larger than the flat we lived in before. You’d love it.\n\nI like my new neighbours. When we arrived, they came right away and introduced themselves. There‘s only one thing I don’t like. It‘s the distance from here to my school because I have to wake up early.\n\nI think you must come to my new place and see all of it. Do you have time this Friday?\n\n See you,\n\n Caroline";

/*
const text =
  "Hello Mary,\n\nHow are you? I am super. I am writing because I want to describe you my house. there live with a family and is very big, a beautiful, a pleasing. At home is ten rooms which have small, big, yellow, white, red, green and blue. Our house have green colour, white windows, brown door and red a roof.\n\n Our new house is about a lot beautiful than our old house. At new house is life nice beacuse is tidy and there are all news and also have beautiful garden and a swimming pool. And red roof and brown door and shote windows are nice also.\n\n I am invite you also to my home because I describe you my house.\n\n See you soon,\n\nTom";

*/

const topics = {
  topic: "Unformal letter for friend Lucy about reccently bought house in USA",

  topicKeys: ["letter", "move", "house", "visit", "new", "past"],

  paragraphTopics: [
    "Tell about reason, why are you writing a letter.",
    "Describe new house a lot and compare him with your recent house.",
    "Tell about good thinks and possitives of new house, also mention negatives and bad things and describe why.",
    "Invite Lucy for visit your house."
  ],

  paragraphKeys: [
    ["reason", "writing", "letter", "why", "explain"],
    ["describe", "new", "house", "past", "compare"],
    ["possitives", "negatives", "house", "describe", "why"],
    ["invite", "invitation", "visit", "home", "future"]
  ]
};

export default topics;

/*
const topics = {
  topic: "Uwo nasa pilots discovered new planet",

  topicKeys: ["space", "nasa", "pilots", "planets", "ship"],

  paragraphTopics: [
    "Tell about reason, why are you writing a letter.",
    "Describe new house a lot and compare him with your recent house.",
    "Tell about good thinks and possitives of new house, also mention negatives and bad things and describe why.",
    "Invite Lucy for visit your house."
  ],

  paragraphKeys: [
    ["reason", "writing", "letter", "why", "explain"],
    ["describe", "new", "house", "past", "compare"],
    ["possitives", "negatives", "house", "describe", "why"],
    ["invite", "invitation", "visit", "home", "future"]
  ]
};
*/
