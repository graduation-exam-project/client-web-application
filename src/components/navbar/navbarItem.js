import { Link } from "preact-router/match";

function NavbarItem(props) {
  return (
    <Link href={props.link}>
      <li class="navbar-item">
        <a class="navbar-link">{props.title}</a>
      </li>
    </Link>
  );
}

export default NavbarItem;
