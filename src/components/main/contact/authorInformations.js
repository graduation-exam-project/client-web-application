import { authorInformations } from "../../../static/thanksInformations";

function AuthorInformations() {
  return (
    <table class="u-full-width">
      <thead>
        <tr key="row_head">
          <th>Name</th>
          <th>E-mail</th>
          <th class="mobile-hidden">Phone</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{authorInformations.name}</td>
          <td>{authorInformations.email}</td>
          <td class="mobile-hidden">{authorInformations.phone}</td>
        </tr>
      </tbody>
    </table>
  );
}

export default AuthorInformations;
