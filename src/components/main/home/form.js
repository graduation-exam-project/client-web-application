import React, { Component } from "react";
import axios from "axios";
import topics from "../../../static/topics";

const uploadUrl = "http://localhost:8001/api";
const fileUrl = "http://localhost:8002/api";
const imageUrl = "http://localhost:8003/api";
const resultUrl = "http://localhost:8005/api";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = { message: "Uploading file to server...", view: 0, data: {} };
    this.handleSendFiles = this.handleSendFiles.bind(this);
  }

  async handleSendFiles(file) {
    const split = file.name.split(".");

    if (
      split[split.length - 1] === "pdf" ||
      "docx" ||
      "txt" ||
      "jpg" ||
      "jpeg" ||
      "png"
    ) {
      this.props.changeView(1);
      this.setState({ view: 1 });
      await setTimeout(async () => {
        /*
        const data = await new FormData();

        await data.append("file", file, file.name);

        console.log(data.file, data.getAll("file"));

        const response = await fetch(uploadUrl, {
          method: "POST",
          body: data,
          headers: {
            "Content-Type": "multipart/form-data",
            "cache-control": "no-cache"
          }
        })

        console.log(response);

        const req = await response.json();
        console.log(req);
        */
        const req = "test.txt";
        const res = req.split(".");

        if (
          res[res.length - 1] === "pdf" ||
          res[res.length - 1] === "docx" ||
          res[res.length - 1] === "txt"
        ) {
          this.setState({ message: "Reading text from file..." });

          const data = await axios.get(fileUrl + "?filename=" + req);
          await setTimeout(async () => {
            this.setState({ message: "Evaluating results..." });

            const final = await axios.post(resultUrl, {
              ...topics,
              text: data.data.data
            });

            await setTimeout(async () => {
              this.setState({
                message: "Evaluating done!",
                view: 2,
                data: final.data
              });
            }, 2000);
          }, 2000);
        } else if (
          res[split.length - 1] === "jpg" ||
          res[res.length - 1] === "jpeg" ||
          res[res.length - 1] === "png"
        ) {
          this.setState({ message: "Reading text from image..." });

          const data = await axios.get(imageUrl + "?filename=" + req);
          await setTimeout(async () => {
            this.setState({ message: "Evaluating results..." });

            const final = await axios.post(resultUrl, {
              ...topics,
              text: data.data.data
            });

            await setTimeout(async () => {
              this.setState({
                message: "Evaluating done!",
                view: 2,
                data: final.data
              });
            }, 2000);
          }, 2000);
        }
      }, 2000);
    } else {
      alert("Not supported file!");
      this.forceUpdate();
    }
  }

  render() {
    console.log(this.state);
    if (this.state.view === 2 && this.state.data) {
      const data = this.state.data;
      return (
        <div class="row">
          <h2>Your results: </h2>
          <h5>{topics.topic}</h5>
          <table class="u-full-width result-table">
            <thead>
              <tr>
                {data.id.map(item => (
                  <th key={"h_" + item + "_" + data.id.indexOf(item)}>
                    {item}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              <tr>
                {data.A.map(item => (
                  <td key={"i_" + item + "_" + data.A.indexOf(item)}>{item}</td>
                ))}
              </tr>
              <tr>
                {data.B.map(item => (
                  <td key={"ii_" + item + "_" + data.B.indexOf(item)}>
                    {item}
                  </td>
                ))}
              </tr>
            </tbody>
          </table>
        </div>
      );
    } else if (this.state.view === 1)
      return (
        <div class="row">
          <div class="atom">
            <div class="center"></div>
            <div class="ring"></div>
            <div class="ring"></div>
            <div class="ring"></div>
            <div class="e"></div>
            <div class="e"></div>
            <div class="e"></div>
          </div>
          <h1 class="status-text">{this.state.message}</h1>
        </div>
      );
    else
      return (
        <div class="row">
          <label>Paste your .txt, .docx, .pdf, .jpg or .png file.</label>
          <label class="file column">
            <input
              type="file"
              id="file"
              aria-label="File browser"
              onChange={e => this.handleSendFiles(e.target.files[0])}
            />
            <span class="file-custom"></span>
          </label>
        </div>
      );
  }
}

export default Form;

/*
function Form() {
  const sendFiles = async file => {
    let split = file.name.split(".");
    let res = await fetch("http://localhost:8001/api", {
        method: "POST",
        body: file
      });

    if (split[split.length - 1] === "pdf" || "docx" || "txt")
      

    else if (split[split.length - 1] === "jpg" || "jpeg" || "png")
      fetch("http://localhost:8001/api", {
        method: "POST",
        body: file
      });
    else alert("Not supported file!");
  };

  return (

  );
}
*/
